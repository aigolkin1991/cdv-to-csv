const Writable = require('constant-db').writable;
const fs = require('fs')

let writer = null

const _checkInput = (filePath, keyPos, cdbPass) => {
    if(!filePath){
        console.error("Now path to file")
        return false
    }
    if(isNaN(keyPos)){
        console.error("Bad key pos", keyPos)
        return false
    }
    if(!fs.existsSync(filePath)){
        console.error("File not found", filePath)
        return false
    }
    if(!cdbPass || cdbPass.length === 0){
        console.error("Cdb pass no specified", cdbPass)
        return false
    }
    return true
}

const BUFF_LENGTH = 1000000
const BUFF = Buffer.alloc(BUFF_LENGTH)

let STR_BUFF = ''
let sumProcessed = 0;

const _processChunk = (chunk, keyPos, cb) => {
    const dataArr = (STR_BUFF+chunk).split('\n')
    const last = dataArr.pop()
    const len = dataArr.length
    STR_BUFF = last

    let el = ''
    let pArr = []
    while(el = dataArr.pop()){
        const rowData = el.split(',')
        const key = rowData[keyPos]
        rowData.splice(keyPos, 1)
        if(!key || key.length === 0){
            console.log("Bad line", el)
        }else{
            pArr.push(writer.put(key,(rowData.length > 1 ? rowData.join(',') : '1')))
        }
    }
    Promise.all(pArr)
    .then(() => {
        console.log("Processed", (sumProcessed+=len))
        cb()
    })
    .catch((err) => {
        console.error("Processing chunk failed", err)
    })
}

const _getChunk = (fd, keyPos) => {
    fs.read(fd, BUFF, 0, BUFF_LENGTH, null, (err, bytesRead, buff) => {
        if(err){
            console.error("Read data error", err)
            fs.close(fd, (err) => {
                if(err){
                    console.error("Close file error", err)
                }
            })
        }else if(bytesRead > 0){
            _processChunk(BUFF.slice(0, bytesRead).toString(), keyPos, () => {
                _readByChunks(fd, keyPos)
            })
        }else{
            console.log("No data left to read")
            fs.close(fd, async (err) => {
                if(err){
                    console.error("Close file error", err)
                }
            })
            writer.close((err) => {
                if(!err) console.log('ALL_DONE')
                else console.error('WRITE_CLOSE_ERROR', err)
            })
        }
    })
}

const _readByChunks = (fd, keyPos) => {
    _getChunk(fd, keyPos)
}

const _processFile = (filePath, keyPos) => {
    fs.open(filePath, (err, fd) => {
        if(err){
            console.error("Fail to open file", filePath, err)
        }else{
            console.log("File openned", fd)
            _readByChunks(fd, keyPos)
        }
    })
}

const main = () => {
    const cvfFile = process.argv[2]
    const keyPos = parseInt(process.argv[3])
    const cdbPass = process.argv[4]
    if(!_checkInput(cvfFile, keyPos, cdbPass)){
        console.log("Usage: node index.js csv_file_path key_postion cdb_file_path")
        return 0
    }
    try{
        writer = new Writable(cdbPass);
    }catch(e){
        console.log("Cant create cdb file at", cdbPass, e)
        return 0
    }
    writer.open((err) => {
        if(!err) _processFile(cvfFile, keyPos)
        else console.log("Can create writeable for cdb", err)
    })
}

main()