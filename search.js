const Readable = require('constant-db').readable;

const getReader = (cdbPath) => {
    const reader = new Readable(cdbPath);
    return new Promise((resolve, reject) => {
        reader.open((err) => {
            if(err){
                reject(err)
            }else{
                const pathArr = cdbPath.split('/')
                const cdbName = pathArr[pathArr.length - 1].replace('.cdb', '')
                resolve({cdbName,reader})
            }
        })
    })
}

const getMultiReader = (cdbPathArr) => {
    const reader = {}
    const pArr = []

    cdbPathArr.forEach(element => {
        pArr.push(getReader(element))
    });
    return Promise.all(pArr)
    .then((readers) => {
        reader.readerMap = []
        for(let v of readers){
            reader.readerMap.push([v.cdbName,v.reader])
        }
        reader.readerMap.sort((a,b) => {
            if(a[0] > b[0]) return 1
            if(a[0] < b[0]) return -1
            return 0 
        })
        reader.get = (key) => {
            let exactReader
            for(exactReader of reader.readerMap){
                if(key > exactReader[0]){
                    continue
                }else{
                    break
                }
            }
            return new Promise((resolve,reject) => {
                exactReader[1].get(key.toUpperCase(), (err,res) => {
                    if(err){
                        reject(err)
                    }else{
                        resolve(res ? res.toString() : false)
                    }
                })
            })
        }
        return reader
    })
}

module.exports = {
    getReader,
    getMultiReader
}